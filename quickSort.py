def quick(x):
    if len(x) <= 1:
        return x
    pivot = x[-1]
    data = x[:-1]
    kiri = []
    kanan = []
    for i in data:
        if i <= pivot:
            kiri.append(i)
        elif i >= pivot:
            kanan.append(i)
    return quick(kiri) + [pivot] + quick(kanan) 

def quickdesc(y):
    if len(y) <= 1:
        return y
    pivot = y[-1]
    data = y[:-1]
    kiri = []
    kanan = []
    for i in data:
        if i <= pivot:
            kanan.append(i)
        elif i >= pivot:
            kiri.append(i)
    return quickdesc(kiri) + [pivot] + quickdesc(kanan)