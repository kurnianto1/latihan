def drawing(jumlah, tujuan):
    depan = tujuan//2
    belakang = (jumlah-tujuan)//2

    if tujuan%2 != 0 and jumlah-tujuan == 1:
        belakang += 1

    if belakang < depan:
        return{'minimal halaman yang dibuka dari belakang' : belakang}
    else:
        return{'minimal halaman yang dibuka dari depan' : depan}