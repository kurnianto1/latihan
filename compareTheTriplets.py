def compare(abi,umi):

    score_abi = 0
    score_umi = 0
    for i in range(len(abi)):
        if abi[i] > umi[i]:
            score_abi += 1
        elif abi[i] < umi[i]:
            score_umi += 1
    return score_abi, score_umi