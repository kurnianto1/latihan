from flask import Flask, request, make_response
import matemaTika
import simpleArraySum
import compareTheTriplets
# import diagonalDifference
import mergeSort
import quickSort
import plusMinus
import drawingBook
import jumpingNumbers
import hallowenSale

app = Flask(__name__)

@app.route('/<data>')
def home(data):
    if data == 'budi':
        return 'halo {}'. format(data)
    else:
        return 'halo, kamu bukan budi'
@app.route('/add', methods=['POST'])
def add():
    body = request.get_json()
    a = body['a']
    b = body['b']
    result = matemaTika.add(a,b)
    return {'result' : result}

@app.route('/substract', methods=['POST'])
def substract():
    body = request.get_json()
    a = body['a']
    b = body['b']
    result = matemaTika.Substract(a,b)
    return {'result' : result}


@app.route('/array-sum', methods=['POST'])
def arraySum():
    body = request.get_json()
    data = body['data']
    result = simpleArraySum.sum(data)
    return {'sum' : result}

@app.route('/compare-the-triplets', methods=['POST'])
def compare():
    body = request.get_json()
    budi = body['budi']
    ani = body['ani']
    result = compareTheTriplets.compare(budi,ani)
    return {"skor" : result}

# @app.route('/diagonal-difference', methods=['POST'])
# def diagonaldifference():
#     body = request.get_json()
#     data = body['data']
#     result = diagonalDifference.diagonal(data)
#     return {"result" : result}

@app.route('/merge-sort', methods=['POST']) 
def mergesort():
    body = request.get_json()
    data = body['data']
    asc = request.headers.get('ASC')
    
    if asc == 'True':
        result = mergeSort.merge(data)
    elif asc == 'False':
        result = mergeSort.mergedesc(data)
    else:
        return 'salah memasukkan path parameter, gunakan True atau False'
    return {"data urut" : result}

@app.route('/quick-sort/<masukan>', methods=['POST'])
def quicksort(masukan):
    asc = request.headers.get('ASC')
    desc = request.headers.get('DESC')
    body = request.get_json()
    data = body['data']
    
    if masukan == asc:
        result = quickSort.quick(data)
    elif masukan == desc:
        result = quickSort.quickdesc(data)
    else:
        return 'salah memasukkan path parameter, gunakan asc atau desc'
    return {"data" : result}  

@app.route('/plus-minus', methods=['POST'])
def plusminus():
    body = request.get_json()
    data = body['data']
    result = plusMinus.compare(data)
    return {"result compare plus minus" : result}

@app.route('/drawing-book', methods=['POST'])
def drawingbook():
    body = request.get_json()
    jumlah = body['jumlah']
    tujuan = body['tujuan']
    result = drawingBook.drawing(jumlah, tujuan)
    return result

@app.route('/jumping-numbers', methods=['POST'])
def jumpingnumbers():
    body = request.get_json()
    x1 = body['x1']
    v1 = body['v1']
    x2 = body['x2']
    v2 = body['v2']
    result = jumpingNumbers.jumping(x1,v1,x2,v2)
    return result

@app.route('/hallowen-sale', methods=['POST'])
def hallowen():
    body = request.get_json()
    price = body['price']
    disc = body['disc']
    minPrice = body['minPrice']
    money = body['money']
    result = hallowenSale.sale(price,disc,minPrice,money)
    return {"yang bisa dibeli" : result}

if __name__ == '__main__':
    app.run(debug=True)