def compare(data):
    plus, minus, nol = 0, 0, 0

    for i in data:
        if i > 0:
            plus += 1
        elif i < 0:
            minus += 1
        else:
            nol += 1
    plus = plus / len(data)
    minus = minus / len(data)
    nol = nol / len(data)

    return plus, minus, nol