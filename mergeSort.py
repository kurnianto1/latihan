def merge(x):
    # print('Splitting', x)
    if len(x) > 1:
        middle = len(x) // 2
        left = merge(x[:middle])
        right = merge(x[middle :])
        return mergeSort(left, right)
    else:
        return x
def mergeSort(left, right):
    result = []
    i, j = 0, 0
    while (i < len(left)) and (j < len(right)):
        if left[i] < right[j]:
            result.append(left[i])
            i = i + 1
        else:
            result.append(right[j])
            j = j + 1
    while (i < len(left)):
        result.append(left[i])
        i = i + 1
    while (j < len(right)):
        result.append(right[j])
        j = j + 1
    # print('Merging', result)
    return result

def mergedesc(x):
    if len(x) > 1:
        middle = len(x) // 2
        left = mergedesc(x[:middle])
        right = mergedesc(x[middle:])
        return mergeSortDesc(left, right)
    else:
        return x
def mergeSortDesc(left, right):
    result = []
    i, j = 0, 0
    while (i < len(left)) and (j < len(right)):
        if left[i] > right[j]:
            result.append(left[i])
            i = i + 1
        else:
            result.append(right[j])
            j = j + 1
    while (i < len(left)):
        result.append(left[i])
        i = i + 1
    while (j < len(right)):
        result.append(right[j])
        j = j + 1
    return result