def jumping(x1,v1,x2,v2):
    if v1 <= v2:
        return {"berpapasan" : "tidak"}
    
    while x1 < x2:
        x1 += v1
        x2 += v2

    return {"berpapasan" : "ya"} if x1 == x2 else {"berpapasan" : "tidak"} 