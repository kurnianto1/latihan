def sale(price, disc, minPrice, money):
    # jumlah yang bisa dibeli
    n_game = 0
    
    while price <= money:
        n_game += 1
        money = money - price
        price = price - disc

        if price <= minPrice:
            price = minPrice    
    return n_game